package com.api.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.customer.model.Customer;
import com.api.customer.service.CustomerService;

@RestController
@RequestMapping("/api-customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	@GetMapping
	public ResponseEntity<?> findByDocumentNumber(@RequestParam String documentNumber) {
		
		Customer customer = customerService.findByDocumentNumber(documentNumber);
		
		if(customer!=null) {
			return ResponseEntity.ok().body(customer);
		} else {
			return ResponseEntity.ok().body("No se encontro customer");
		}
	}
	
	@GetMapping("/{documentNumber}")
	public Customer findByDocumentNumberPath(@PathVariable String documentNumber) {
		return customerService.findByDocumentNumber(documentNumber);
	}
	
	@PostMapping("/save")
	public Customer saveCustomer(@RequestBody Customer customer) {
		return customerService.saveCustomer(customer);
	}
	
	@PostMapping("/remove/{id}")
	public void removeCustomer(@PathVariable Long id) {
		customerService.removeCustomer(id);
	}
	
}
