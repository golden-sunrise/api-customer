package com.api.customer.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name="Customer")
@Data
public class Customer {

	@Id
	@Column(name="id")
	private Long id;
	
	@Column(name="document_type")
	private String documentType;
	
	@Column(name="document_number")
	private String documentNumber;
	
	@Column(name="name")
	private String name;
	
	@Column(name="phone")
	private String phone;
	
}
