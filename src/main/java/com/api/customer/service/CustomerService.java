package com.api.customer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.customer.model.Customer;
import com.api.customer.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	
	public Customer findByDocumentNumber(String documentNumber) {
		return customerRepository.findByDocumentNumber(documentNumber);
	}
	
	public Customer saveCustomer(Customer customer) {
		return customerRepository.save(customer);
	}
	
	public void removeCustomer(Long id) {
         customerRepository.deleteById(id);
	}
}
